from pathlib import Path
import os


############################## Constants ###############################
SMS_SAMPLE_DICT = dict()
DICT_SPLITTER_SAMPLE = ';'
EXTRACT_KITS_PATH = "PATH"


############################## Functions ###############################
def sampleNameNormaliser(sampleName: str):
    return sampleName.upper().replace('-', '_')

def addAPath(path: Path, lambdaRename, lambdaStrainNum, lambdaCondit=lambda x: True, sms_register= SMS_SAMPLE_DICT):
    filesSplitted = __routine(path, lambdaRename, lambdaStrainNum, lambdaCondit)
    for i in filesSplitted : 
        if i["sample"] in sms_register :
            sms_register[i["sample"]] += DICT_SPLITTER_SAMPLE + i["path"]
        else : 
            sms_register[i["sample"]] = i["path"]


###### Private Functions ###############################
def __routine(path: Path, lambdaRename, lambdaStrainNum, lambdaCondit=lambda x: True):
    def strainCtrl(x):
        x = int(lambdaStrainNum(x))
        assert x == 1 or x == 2, f"strain number error {x} insn't a valid value"
        return x

    def toObject(sample, strain, path, fileOriginalName):
        return {"sample": sample, "strain": strain, "path": path, "file": fileOriginalName}

    return [
        toObject(
            sample    = sampleNameNormaliser(lambdaRename(i)), 
            strain  = strainCtrl(i), 
            path    = f"{path}/{i}", 
            fileOriginalName = i
        )
        for i in os.listdir(path) if lambdaCondit(i)]
    

######################### Providers splitting #########################
provider1_rename = lambda x : x[13:16]
provider1_strain = lambda x : x[17]

provider2_rename = lambda x : x[:3]
provider2_strain = lambda x : x[20]
provider2_condit = lambda x : x[-4:] != ".md5"


############################## Execution ###############################
addAPath(Path("sequencing_provider_1/"), provider1_rename, provider1_strain )
addAPath(Path("sequencing_provider_2/"), provider2_rename, provider2_strain, provider2_condit)

print(SMS_SAMPLE_DICT)